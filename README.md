# Валидатор

## Сборка

```
pip install flit
python -m build
```

## Установка

```
pip install dist/validator-2.0-py3-none-any.whl
```

## Документация

```
make -C docs html
```
