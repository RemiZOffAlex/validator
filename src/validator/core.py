__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging


log = logging.getLogger(__name__)


class Error:
    def __init__(self, message: str = ''):
        self.message = message

    def __repr__(self):
        return "Result validation is error: {}".format(self.message)


class Validator:
    def __init__(self, rules: list = [], debug: bool = False):
        self.rules = rules
        self.debug = debug
        if self.debug:
            log.info('rules: {}'.format(rules))

    def __call__(self, value):
        result = True
        for rule in self.rules:
            if self.debug:
                log.info('rule: {}'.format(rule))
            result = rule(value)
            if isinstance(result, Error):
                return result
        return Result()


class Result:
    def __init__(self, message: str = ''):
        self.message = message

    def __repr__(self):
        return "Result validation is ok: {}".format(self.message)
