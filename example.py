#!/usr/bin/env python3

__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from validator.core import Validator, Error
from validator.presets.common import EqualTo, MinLength


password = 'PASSWORD'
confirm = 'PASSWORD'

validator = Validator([
    MinLength(1, message="password is to short use at least 1 character"),
    EqualTo(confirm, message="passwords mismatch")
])
result = validator(password)
print(result)

if isinstance(result, Error):
    logging.error(result.message)
    raise ValueError(result.message)
