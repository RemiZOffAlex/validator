__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from ..core import Result, Error

log = logging.getLogger(__name__)


class EqualTo:
    def __init__(self, value, message: str = ''):
        self.value = value
        self.message = message

    def __call__(self, value):
        if self.value != value:
            return Error(self.message)
        return Result()


class MaxLength:
    def __init__(self, value, message: str = ''):
        self.limit = value
        self.message = message

    def __call__(self, value):
        if len(value) > self.limit:
            return Error(self.message)
        return Result()


class MinLength():
    def __init__(self, value, message: str = ''):
        self.limit = value
        self.message = message

    def __call__(self, value):
        if len(value) < self.limit:
            return Error(self.message)
        return Result()
