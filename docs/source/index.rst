.. Validator documentation master file, created by
   sphinx-quickstart on Sat Feb 19 01:17:02 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Валидатор
=========

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   example


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Сборка
------

.. code::

   pip install flit
   python -m build

Установка
---------

.. code::

   pip install dist/validator-2.0-py3-none-any.whl
